<style type="text/css">
	.gambok{
		margin-top: 7%;
	}
	.judul{
		margin-top: 7%;
	}
	.judul h1{
		font-family: Bombardier;
		color:#ffffff;
		font-size:9vh;
	}
	.judul h4{
		color:#f3b229;
		font-weight: 500;
		text-transform: uppercase;
		font-family: calibri light;
	}
	
	.tombol{
		color:black;
		font-family: calibri light;
		font-weight: 800;
		text-transform: uppercase;
		font-size: 14px;
		border-radius: 0;
	}
	.jarak-login{
	padding-right:10px;
	}
	.next{
		color:white;
		font-family: sans-serif;
		font-weight: 600;
		text-transform: uppercase;
		font-size: 14px;
		border-radius: none;
		border: 2px solid #e96b20;
		background: transparent;
		margin: 5%;
		transition: 0.8s all;
	}
	.next:hover{
		background: #e96b20;
		color: black;
		transition: 0.8s all;
	}
	.jarak-next{
		padding-left: 10px;
	}

	.input{
		margin:3%;
	}
	.input-umur{
		margin:3%;
		width:25%;
	}
	.input-group .form-control:last-child, .input-group-addon:last-child, .input-group-btn:last-child > .btn, .input-group-btn:last-child > .btn-group > .btn, .input-group-btn:last-child > .dropdown-toggle, .input-group-btn:first-child > .btn:not(:first-child), .input-group-btn:first-child > .btn-group:not(:first-child) > .btn {
	    border-top-left-radius: 0;
	    border-bottom-left-radius: 0;
	    background: #3c3c3c;
		border-color: #3c3c3c;
	    width: 70%;
	    color: white;
	}
	.input-group-addon:first-child {
    background: #3c3c3c;
	border-color: #3c3c3c;
	color: white;
	border-right: 1px solid #474242f4;
	}
</style>

<div class="col-sm-8 col-md-4 col-md-offset-1 gambok">
	<img src="<?=base_url();?>asset/bioskop/gambar/game-box_69.jpg" style="width: 100%">
</div>

<div class="col-sm-4 col-md-6 col-md-offset-1 judul">
	<h1>Ni no Kuni II: REVENANT KINGDOM</h1>
			<div class="input-group input">
				<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
				<input type="text" class="form-control form" placeholder="Username">
			</div>
		
			<div class="input-group input">
				<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
				<input type="password" class="form-control form" placeholder="ID Account">
			</div>

			<div class="input-group input-umur">
				<span class="input-group-addon"><span class="glyphicon glyphicon-bullhorn"></span></span>
				<input type="number" class="form-control form" placeholder="Age">
			</div>

			<div class="input-group input">
				<span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
				<input type="email" class="form-control form" placeholder="Email">
			</div>

	<button type="submit" class="btn  next">
		 Buy Now
	</button>
</div>

