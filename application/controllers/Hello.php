<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hello extends CI_Controller {

	public function index()
	{
		if($this->session->userdata('login')!=TRUE){
			redirect('hello/login','refresh');
		}
		$data['konten']="isi";
		$this->load->view('home', $data, FALSE);
	}
	public function boking()
	{
		$data['konten']="boking";
		$this->load->view('home', $data);
	}
	/*menampilkan film*/
	public function jadwal()
	{
		$this->load->model('m_hello');
		$data['jadwal']=$this->m_hello->get_data_film();
		$data['konten']="jadwal";
		$this->load->view('home', $data);
	}
	public function detail_jadwal($id_film)
	{
		$this->load->model('m_hello');//meng include kan model 
		$data['tampil_detail']=$this->m_hello->detail_data($id_film);
		$data ['konten']="detail";
		$this->load->view('home',$data);
	}
	/*kritik*/
	public function kritik()
	{
		$data['konten']="kritik";
		$this->load->view('home', $data);
	}
	/*pesan*/
	public function spec()
	{
		$data['konten']="spec";
		$this->load->view('home', $data);
	}

	/*login*/
	public function login()
	{
		$this->load->view('login');
	}
	public function masuk()
	{
		if($this->input->post('cek')){
			$this->form_validation->set_rules('username', 'username', 'trim|required');
			$this->form_validation->set_rules('password', 'password', 'trim|required');
			if ($this->form_validation->run() == TRUE) {
				$this->load->model('m_hello');
				if($this->m_hello->cek_user()->num_rows()>0){
					$data_user=$this->m_hello->cek_user()->row();
					$array = array('login'=>TRUE,'username'=>$data_user->username,'password'=>$data_user->password);
					$this->session->set_userdata($array);
					redirect('hello','refresh'); 
				}else{
					$this->session->set_flashdata('pesan', 'username and password wrong');
					redirect('hello/login','refresh');
				}
			} else {
				$this->session->set_flashdata('pesan', validation_errors());
				redirect('hello/login','refresh');
			}
		}
	}

	/*register*/
	public function register()
	{
		$this->load->view('register');
	}
	public function simpan()
	{
		if($this->input->post('submit')){
			$this->form_validation->set_rules('nama', 'nama lengkap', 'trim|required');
			$this->form_validation->set_rules('username', 'username', 'trim|required');
			$this->form_validation->set_rules('password', 'password', 'trim|required');

			if ($this->form_validation->run() == TRUE) {
				$this->load->model('m_hello');
				$proses=$this->m_hello->simpan_data();
				if($proses){
					$this->session->set_flashdata('pesan', 'sucsses');
					redirect('hello/login');
				}else{
					$this->session->set_flashdata('pesan', 'wrong');
					redirect('hello/register');
				}
			} else {
				$this->session->set_flashdata('pesan',validation_errors());
				redirect('hello/register');
			}
		}
	}

	/*logout*/
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('hello/login','refresh');
	}

}
