<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

	public function index() 
	{
		$data['konten']="show_cart";
		$this->load->view('home', $data, FALSE);	
	}
	public function addcart($id_film)
	{
		$this->load->model('m_hello');
		$detail=$this->m_hello->detail_data($id_film);
		$data = array(
			'id'      => $detail->id_film,
			'qty'     => 1,
			'price'   => 2000,
			'name'    => $detail->nama_film,
			'options' => array()
		);
		
		$this->cart->insert($data);
		redirect('hello/detail_jadwal/'.$id_film,'refresh');
	}
	public function hps_cart($id)
	{
		$data = array(
			'rowid' => $id,
			'qty'   => 0
		);
		
		$this->cart->update($data);
		redirect('cart','refresh');
	}
	public function simpan()
	{
		if ($this->input->post('simpan')) {
			$this->load->model('m_cart');
			$id_nota=$this->m_cart->simpan_cart();
			if ($id_nota >0) {
				redirect('cart/pembayaran/'.$id_nota,'refresh');
			}else{
				redirect('cart','refresh');
			}
		}
	}

}

/* End of file Cart.php */
/* Location: ./application/controllers/Cart.php */