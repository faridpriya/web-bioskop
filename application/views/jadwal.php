<style>
.title{
	background: #ffffff;
	margin-top: 100px;
	height: auto;
}
.title h1{
	font-family: sans-serif;
	font-size: 28px;
	color: #e3672a;
	font-weight: 400;
	line-height: 32px;
}
.fix{
	background: black;
	transition: 0.3s;
}
.jadwal{
	background: #e3e3e3;
	height: auto;
}
.isijadwal1{
	margin-top: 15px;
}
.caption h1{
	color: #000000;
	text-transform: uppercase;
	font-size: 14px;
	font-weight: 700;

</style> 


<div class="container-fluid title">
	<h1>Browse Film</h1>
</div>
<div class="container-fluid jadwal">

<?php
foreach ($jadwal as $film) {
?>
		<div class="col-sm-6 col-md-4">
			<a href ="<?= base_url('index.php/hello/detail_jadwal/'.$film->id_film)?>">
			<img src="<?=base_url()?>asset/bioskop/gambar/<?=$film->gambar_film?>"></a>
			<div class="caption">
				<h1><?= $film->nama_film?></h1>
			</div>
		</div>
<?php } ?>
	
</div>



<script type="text/javascript">
	$(document).ready(function(){
	var navigasi=$(".title").offset().top;
	var sticky = function(){
		var scrollTop = $(window).scrollTop();
		if(scrollTop>navigasi){
			$(".navbar-inverse").addClass("fix");
		}else{
			$(".navbar-inverse").removeClass("fix");
		}
	}
sticky();
$(window).scroll(function(){
sticky();
});
});
</script>