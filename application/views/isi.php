	<div id="myCarousel" class="carousel carousel-fade slide" data-ride="carousel" data-interval="3000">
    <div class="carousel-inner" role="listbox">
      <div class="item active background a">
      	<div class="covertext">
		    <div class="col-lg-10" style="float:none; margin:0 auto;">
		      <p class="title"><img src="<?=base_url();?>asset/bioskop/gambar/game-logo_61.png" style="width: 200px"></p>
		      <h3 class="subtitle">COMING JANUARY 26, 2018</h3>
		      <p class="subtitle2">message tickets from now only in this web</p>
		    </div>
		    <div class="col-md-8 col-xs-offset-1 explore">
		      <a href="<?=base_url();?>asset/bioskop/#"><button type="button" class="btn btn-lg explorebtn">Booking-Now</button></a>
		    </div>
		</div>
      </div>

      <div class="item background b">
      	<div class="covertext">
		    <div class="col-lg-10" style="float:none; margin:0 auto;">
		      <p class="title"><img src="<?=base_url();?>asset/bioskop/gambar/game-logo_SyPCqYwspZJJ_0.png" style="width: 230px; margin-bottom: -20px;"></p>
		      <h3 class="subtitle">NOW AVAILABLE ON CINEMA 21</h3>
		      <p class="subtitle2">message tickets from now only in this web</p>
		    </div>
		    <div class="col-md-8 col-xs-offset-1 explore">
		      <a href="<?=base_url();?>asset/bioskop/#"><button type="button" class="btn btn-lg explorebtn">Booking-Now</button></a>
		    </div>
		</div>
      </div>
    </div>
</div>
	<div class="container-fluid isi">
		<div class="row gambar">
		<div class="col-md-6">
			<img src="<?=base_url();?>asset/bioskop/gambar/luffy-7477064318.png" style="width:200%;margin-left: -210px">
		</div>
		<div class="col-md-4 col-md-offset-1 keterangan">
			<h1>STEP INTO THE OFFICIAL WORLD OF ANIME</h1>
			<p class="deskripsi">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Qui non moveatur et offensione turpitudinis et comprobatione honestatis? Quid igitur, inquit, eos responsuros putas? Si sapiens</p>
			<span class="tombol"><a href="<?=base_url();?>asset/bioskop/login.html"><button type="button" class="btn btn-lg tombolbtn">LOGIN</button></a></span>
			<span class="tombol2"><a href="<?=base_url();?>asset/bioskop/#"><button type="button" class="btn btn-lg explorebtn">REGISTER</button></a></span>
		</div>
		</div>
	</div>

	<div class="container-fluid tempat_jadwal">
		<h1 class="judul_jadwal">MOST <br>POPULAR</h1>
		<p class="keterangan_jadwal">Check out the latest games just here.</p>

		<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist">
		  <li class="active"><a href="<?=base_url();?>asset/bioskop/#1" role="tab" data-toggle="tab">I</a></li>
		  <li><a href="<?=base_url();?>asset/bioskop/#2" role="tab" data-toggle="tab">II</a></li>
		</ul>

		<!-- Tab panes -->
		<div class="tab-content">
		  <div class="tab-pane fade in active" id="1">
		  	<div class="row jarak_atas">
				  <div class="col-sm-6 col-md-3">
					<a href="<?=base_url();?>asset/bioskop/#film1" data-toggle="modal" class="thumbnail bgfilm">
					<img src="<?=base_url();?>asset/bioskop/gambar/film1.jpg"></a>
				</div>
				 <div class="col-sm-6 col-md-3">
					<a href="<?=base_url();?>asset/bioskop/#film2" data-toggle="modal" class="thumbnail bgfilm">
					<img src="<?=base_url();?>asset/bioskop/gambar/film2.jpg"></a>
				</div>
				 <div class="col-sm-6 col-md-3">
					<a href="<?=base_url();?>asset/bioskop/#film3" data-toggle="modal" class="thumbnail bgfilm">
					<img src="<?=base_url();?>asset/bioskop/gambar/film4.jpg"></a>
				</div>
				 <div class="col-sm-6 col-md-3">
					<a href="<?=base_url();?>asset/bioskop/#film4" data-toggle="modal" class="thumbnail bgfilm">
					<img src="<?=base_url();?>asset/bioskop/gambar/film5.jpg"></a>
				</div>
			</div>
		  </div>


		  <div class="tab-pane fade" id="2">
		  	<div class="row jarak_atas">
				  <div class="col-sm-6 col-md-3">
					<a href="<?=base_url();?>asset/bioskop/#film5" data-toggle="modal" class="thumbnail bgfilm">
					<img src="<?=base_url();?>asset/bioskop/gambar/film6.jpg"></a>
				</div>
				 <div class="col-sm-6 col-md-3">
					<a href="<?=base_url();?>asset/bioskop/#film1" data-toggle="modal" class="thumbnail bgfilm">
					<img src="<?=base_url();?>asset/bioskop/gambar/film1.jpg"></a>
				</div>
			</div>
		  </div>
	
		</div>
	</div>

	<div class="container-fluid hal_rating">
		<div class="row">
			<div class="col-md-3">
				<img src="<?=base_url();?>asset/bioskop/gambar/signup-character-01-07121ceb97.png" style="width: 100%">
			</div>

			<div class="col-md-6">
			<h1 class="text-center judul_rating">DISCOUNT 50% FOR ALL GAMES </h1>
			<p class="text-center deskripsi_rating">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Qui non moveatur et offensione turpitudinis et comprobatione honestatis? Quid igitur,</p>
			<p id="hitungan"></p>
			</div>

			<div class="col-md-3">
				<img src="<?=base_url();?>asset/bioskop/gambar/signup-character-02-718c1e4eb4.png" style="width: 100%">
			</div>
		</div>
	</div>

	<div class="container-fluid bgcopy">
		<p class="text-center copy"> &copy 2012-2017 Z-SMART CINEMAX Entertainment Indonesia Inc. All Rights Reserved.</p>
	</div>