<style type="text/css">
	.container{
		background: #f9f9f9;
		margin-top: 10%;
		border-radius: 10px;
	}

</style>
<div class="container">
<div class="col-md-12">
	<h2>Daftar Peminjaman Buku</h2>
<form action="<?=base_url('index.php/cart/simpan')?>" method="post">
<table class="table table-hover tabel-striped">
	<tr>
		<th>Nama Film</th>
		<th>Qty</th>
		<th>Harga</th>
		<th>Subtotal</th>
		<th>Aksi</th>
	</tr>
	<?php
	foreach($this->cart->contents()as $items){
	?>
	<tr>
		
		<td>
			<input type="hidden" name="id_film[]" value="<?=$items['id']?>">
			<input type="hidden" name="qty[]" value="<?=$items['qty']?>">
			<?= $items['name']?>
		</td>
		<td><?= $items['qty']?></td>
		<td><?= $items['price']?></td>
		<td><?= $items['subtotal']?></td>
		<td><a href="<?=base_url('index.php/cart/hps_cart/'.$items['rowid'])?>" onclick="return confirm('anda yakin?')">Hps</a></td>
	</tr>
	<?php
	}
	?>
	<tr>
		<input type="hidden" name="grandtotal" value="<?=$this->cart->total()?>">
		<th colspan="4">Grand Total</th>
		<td><?= $this->cart->total()?></td>
		<td></td>
	</tr>
</table>
<input type="submit" name="simpan" value="CHECK-OUT" class="btn btn-success">
</form>

	</div>
</div> 
