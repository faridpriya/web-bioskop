
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_hello extends CI_Model {

	public function get_data_film()
	{
		return $this->db->get('film')->result();
	}
	public function detail_data($id)
	{
		return $this->db->where('id_film', $id)
					 ->get('film')->row();
	}
	/* proses menyiimpan data register*/
	public function simpan_data()
	{
		$nama		= $this->input->post('nama');
		$username	= $this->input->post('username');
		$password	= $this->input->post('password');
		$data = array('nama_pelanggan'=>$nama,'username'=>$username,'password'=>md5($password)); 
		return $this->db->insert('pelanggan',$data);
	}
	public function cek_user()
	{
		return $this->db
					->where('username',$this->input->post('username'))
					->where('password',md5($this->input->post('password')))
					->get('pelanggan');
	}

}

/* End of file M_hello.php */
/* Location: ./application/models/M_hello.php */