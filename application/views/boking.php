<style type="text/css">
	.gambok{
		margin-top: 7%;
	}
	.judul{
		margin-top: 7%;
	}
	.judul h1{
		font-family: Bombardier;
		color:#ffffff;
		font-size:9vh;
	}
	.judul h4{
		color:#f3b229;
		font-weight: 500;
		text-transform: uppercase;
		font-family: calibri light;
	}
	.judul p{
		font-family: sans-serif;
		font-size: 22px;
		margin-top: 20px;
	}
	.judul h2{
		font-family: sans-serif;
		font-weight: 700;
		color: white;
		font-size: 7vh;
		float: left;
	}
	.list li{
		font-family: sans-serif;
		font-size: 18px;
	}
	.tombol{
		color:black;
		font-family: calibri light;
		font-weight: 800;
		text-transform: uppercase;
		font-size: 14px;
		border-radius: 0;
	}
	.jarak-login{
	padding-right:10px;
	}
	.next{
		color:white;
		font-family: sans-serif;
		font-weight: 600;
		text-transform: uppercase;
		font-size: 14px;
		border-radius: none;
		border: 2px solid #e96b20;
		background: transparent;
		margin: 5%;
		transition: 0.8s all;
	}
	.next:hover{
		background: #e96b20;
		color: black;
		transition: 0.8s all;
	}
	.jarak-next{
		padding-left: 10px;
	}

</style>

<div class="col-sm-8 col-md-4 col-md-offset-1 gambok">
	<img src="<?=base_url();?>asset/bioskop/gambar/game-box_69.jpg" style="width: 100%">
</div>

<div class="col-sm-4 col-md-6 col-md-offset-1 judul">
	<h1>Ni no Kuni II: REVENANT KINGDOM</h1>
	<h4>PLATFORM</h4>
	<button type="submit" class="btn btn-warning tombol">
		<span class="glyphicon glyphicon-credit-card jarak-login"></span> PlayStation®4 
	</button>
	<button type="submit" class="btn btn-success tombol">
		<span class="glyphicon glyphicon-blackboard jarak-login"></span> PC 
	</button>

	<p>order the Collector's Edition to receive:</p>
	<ul class="list">
		<li>Premium Edition content</li>
		<li>Ni no Kuni II Visual Arts Book</li>
		<li>Chibi Mechanical Rotating Diorama</li>
		<li>3D Papercraft Display Cas</li>
	</ul>
	<h2>$49.95</h2>

	<a href="<?=base_url();?>index.php/hello/spec"><button type="submit" class="btn  next">
		 Buy Now
	</button></a>
</div>

