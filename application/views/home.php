<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title></title>
		<link href="<?=base_url();?>asset/bioskop/css/bootstrap.css" rel="stylesheet" type="text/css">
		<link href="<?=base_url();?>asset/bioskop/css/style_home.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="<?=base_url();?>asset/bioskop/js/jquery.js"></script>
		<script type="text/javascript" src="<?=base_url();?>asset/bioskop/js/bootstrap.js"></script> 
	</head>
	<body>
		<nav class="navbar navbar-inverse navbar-fixed-top lebar_navbar" role="navigation">
			<div class="container-fluid">
			    <div class="navbar-header">
					 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					 </button>
					<a class="navbar-brand" href="<?=base_url();?>asset/bioskop/#"><a href="<?=base_url();?>index.php/hello/index"><img src="<?=base_url();?>asset/bioskop/gambar/brand2.png" style="margin-top: 3%;"></a></a>
				</div>
				<div class="collapse navbar-collapse  jarak_menu" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav  menu_kiri">
					<li><a href="<?=base_url();?>asset/bioskop/#">NEWS</a></li>
					<li><a href="<?=base_url();?>asset/bioskop/#">COMPANY</a></li>
					<li><a href="<?=base_url();?>index.php/hello/jadwal">SCHEDULE</a></li>
					<li><a href="<?=base_url();?>index.php/hello/kritik">EVENT</a></li>
					<li><a href="<?= base_url('index.php/cart')?>">PESAN
						<i class="fa fa-shopping-cart"></i>
              			<span class="label label-success"><?=$this->cart->total_items()?></span> 
              			</a></li>
					<li><a href="<?=base_url();?>index.php/hello/logout">LOGOUT</a></li>
				</ul>
				</div>
			</div>
		</nav>

	
	<?php 
	$this->load->view($konten);?>
 

	


		<!--POPUP1-->
	<div id="film1" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
    <div class="modal-content">
      
	  <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Booking ticket</h4>
      </div>
      <div class="modal-body">
      	<div class="row">
      		<div class="col-md-4">
		       <img src="<?=base_url();?>asset/bioskop/gambar/film1.jpg">
		   </div>
		   <div class="col-md-6 col-md-offset-2">
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Qui non moveatur et offensione turpitudinis et comprobatione honestatis? Quid igitur, inquit, eos responsuros putas? Si sapiens, ne tum quidem miser, cum ab Oroete, praetore Darei, in crucem actus est. Indicant pueri, in quibus ut in speculis natura cernitur. Duo Reges: constructio interrete. Hoc mihi cum tuo fratre convenit. Dolor ergo, id est summum malum, metuetur semper, etiamsi non aderit; Quae cum dixisset, finem ille.

		   </div>
	   </div>
      </div>
      <div class="modal-footer">
        <a href="<?=base_url();?>index.php/hello/boking"><button type="button" class="btn btn-primary">Booking-Now</button></a>
      </div>
    </div>
    </div>
	</div>




	</body>
</html>

<script>
// Set the date we're counting down to
var countDownDate = new Date("November 21, 2017 07:30:25").getTime();
// Update the count down every 1 second
var x = setInterval(function() {
    // Get todays date and time
    var now = new Date().getTime();
    // Find the distance between now an the count down date
    var distance = countDownDate - now;
    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    // Output the result in an element with id="demo"
    document.getElementById("hitungan").innerHTML = days + "d " + hours + "h "
    + minutes + "m " + seconds + "s ";
    // If the count down is over, write some text 
    if (distance < 0) {
        clearInterval(x);
        document.getElementById("hitungan").innerHTML = "OPENING";
    }
}, 1000);


$(document).ready(function(){
	var navigasi=$(".carousel").offset().top;
	var sticky = function(){
		var scrollTop = $(window).scrollTop();
		if(scrollTop>navigasi){
			$(".navbar-inverse").addClass("fix");
		}else{
			$(".navbar-inverse").removeClass("fix");
		}
	}
sticky();
$(window).scroll(function(){
sticky();
});
});

</script>