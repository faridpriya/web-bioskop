<style type="text/css">
	.reg h5{
		color: #81998d;
		transition: 0.5s;
		text-decoration: none;
	}
	.reg a h5:hover{
		color: #337ab7;
		transition: 0.5s;
		text-decoration: none;
	}
	.alert-danger {
    color: #a94442;
    background-color: #e8bcbc;
    margin-top: 20px;
	}
</style>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title></title>
		<link href="<?=base_url();?>asset/bioskop/css/bootstrap.css" rel="stylesheet" type="text/css">
		<link href="<?=base_url();?>asset/bioskop/css/style_login.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="<?=base_url();?>asset/bioskop/js/jquery.js"></script>
		<script type="text/javascript" src="<?=base_url();?>asset/bioskop/js/bootstrap.js"></script> 
	</head>
	<body>
		<div class="header_utama">
			<div class="col-md-6 col-md-offset-1 logo">
				<img src="<?=base_url();?>asset/bioskop/gambar/game-box_70.jpg">
			</div>
		</div>
		<div class="header1">
			
			<div class="col-md-6 col-md-offset-3">
				<img src="<?=base_url();?>asset/bioskop/gambar/logo.png">
			</div>

   			<form action="<?=base_url('index.php/hello/masuk')?>" method="post">
			<div class="col-md-8 col-md-offset-2">
			<div class="input-group">
				  <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
				  <input type="text" class="form-control form" placeholder="Username" name="username">
			</div>
		
				<div class="input-group jarak-password">
				  <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
				  <input type="password" class="form-control form" placeholder="Password" name="password">
				</div>

				<input  name="cek" type="submit" class="btn btn-primary btn-block btn-flat" value="Sign In">
				<input type="hidden" name="cek" value="1">

				<div class="reg">
				<a href="<?=base_url('index.php/hello/register')?>"><h5>Don't Have Acount</h5></a>
				</div>

				<?php if($this->session->flashdata('pesan')!=null):?>
  				<div class="alert alert-danger"><?=$this->session->flashdata('pesan');?></div>
				<?php endif ?>
			</div>
			</form>

		</div>
	</body>
</html>
	